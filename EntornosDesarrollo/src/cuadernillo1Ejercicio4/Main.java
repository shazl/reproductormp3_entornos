package cuadernillo1Ejercicio4;

import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		// Pruebas del programa del reproductor MP3 que mand� el profesor en el
		// cuadernillo de entornos

		Reproductor r1 = new Reproductor("Marcos", "Adidas", "Al", LocalDate.of(2017, 5, 24));

		Lista l1 = new Lista("C1", "Rock");
		Lista l2 = new Lista("Piano", "Instrumentos");

		Cancion c1 = new Cancion(3.52, "Partners In Crime", "Set It Off", LocalDate.of(2005, 4, 7));
		Cancion c2 = new Cancion(3.52, "Wolf in Sheeps Clothes", "Set It Off", LocalDate.of(2012, 7, 18));
		Cancion c3 = new Cancion(3.52, "Machine", "All Good Things", LocalDate.of(2015, 9, 20));
		Cancion c4 = new Cancion(3.52, "Lycoris Radiata", "Spikes", LocalDate.of(2018, 8, 25));

		try {

			r1.addLista(l1);
			r1.addLista(l2);

			l1.addCancion(c1);
			l1.addCancion(c2);
			l1.addCancion(c3);

			l2.addCancion(c4);

			// M�todos Reproductor
			r1.play();
			r1.stop();
			r1.playAll();

			// M�todos Lista
			l1.playAll();

		} catch (Exception e) {
			System.out.println("Hubo un error en el programa");
		} finally {
			System.out.println("Fin del programa");
		}

	}
}