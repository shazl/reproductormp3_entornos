package cuadernillo1Ejercicio4;

import java.util.ArrayList;

public class Lista {

	private String nombre;
	private String categoria;
	private ArrayList<Cancion> listaCanciones = new ArrayList<Cancion>();

	public Lista(String nombre, String categoria) {
		super();
		this.nombre = nombre;
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public ArrayList<Cancion> getListaCanciones() {
		return listaCanciones;
	}

	public void setListaCanciones(ArrayList<Cancion> listaCanciones) {
		this.listaCanciones = listaCanciones;
	}

	public void addCancion(Cancion item) {

		try {

			listaCanciones.add(item);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void totalCanciones() {

	}

	public void totalDuracion() {

	}
	
	public void playAll() {

		try {

			// Recorre las canciones de la lista
			for (int i = 0; i < listaCanciones.size(); i++) {

				System.out.println("Reproduciendo de la lista "+nombre+": "+listaCanciones.get(i).getTitulo());

			}

			System.out.println("");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void mostrarAll() {

	}
}