package cuadernillo1Ejercicio4;

import java.time.LocalDate;
import java.util.ArrayList;

public class Reproductor {

	private String propietario;
	private String marca;
	private String modelo;
	private LocalDate f_fabricacion;
	private ArrayList<Lista> grupoListas = new ArrayList<Lista>();

	public Reproductor(String propietario, String marca, String modelo, LocalDate f_fabricacion) {
		super();
		this.propietario = propietario;
		this.marca = marca;
		this.modelo = modelo;
		this.f_fabricacion = f_fabricacion;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public LocalDate getF_fabricacion() {
		return f_fabricacion;
	}

	public void setF_fabricacion(LocalDate f_fabricacion) {
		this.f_fabricacion = f_fabricacion;
	}

	public ArrayList<Lista> getGrupoListas() {
		return grupoListas;
	}

	public void setGrupoListas(ArrayList<Lista> grupoListas) {
		this.grupoListas = grupoListas;
	}

	public void addLista(Lista item) {

		try {

			grupoListas.add(item);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void play() {

		try {

			System.out.println("Playing...");

			System.out.println("");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void stop() {

		try {

			System.out.println("Stop / Pause");

			System.out.println("");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void totalLista() {

	}

	public void playAll() {

		try {

			// Recorre las listas
			for (int i = 0; i < grupoListas.size(); i++) {

				// Recorre las canciones de la lista
				for (int y = 0; y < grupoListas.get(i).getListaCanciones().size(); y++) {

					System.out.println("Reproduciendo " + grupoListas.get(i).getListaCanciones().get(y).getTitulo());

				}
			}

			System.out.println("");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void mostrarListas() {

	}
}