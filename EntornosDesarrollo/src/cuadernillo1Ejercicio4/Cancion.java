package cuadernillo1Ejercicio4;

import java.time.LocalDate;

public class Cancion {

	private double duracion;
	private String titulo;
	private String artista;
	private LocalDate fecha;

	public Cancion(double duracion, String titulo, String artista, LocalDate fecha) {
		super();
		this.duracion = duracion;
		this.titulo = titulo;
		this.artista = artista;
		this.fecha = fecha;
	}

	public double getDuracion() {
		return duracion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
}